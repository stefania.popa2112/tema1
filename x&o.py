# Jocul "x si 0"
print("Pozsitiile din tabela sunt: \n")
print(" 0| 1| 2\n","3| 4| 5\n","6| 7| 8\n")

pozitii = [0, 1, 2, 3, 4, 5, 6, 7, 8]
aleg = []
pozitie_aleasa1 = 0
pozitie_aleasa2 = 0

print("un jucator va alege x si celalalt va alege 0")

while True:
    jucator1 = input("Apasa x: ")
    if(jucator1.upper() == "X"):
        jucator2 = '0'
        break
    else:
        pass

def tabela():
        print(pozitii[0], "|", pozitii[1], "|", pozitii[2])
        print("--------")
        print(pozitii[3], "|", pozitii[4], "|", pozitii[5])
        print("--------")
        print(pozitii[6], "|", pozitii[7], "|", pozitii[8])

def verif(char, poz1, poz2, poz3):
    if pozitii[poz1] == char and pozitii[poz2] == char and pozitii[poz3] == char:
            return True

def verificare(char):
        if verif(char,0,1,2) or verif(char,3,4,5) or verif(char,6,7,8) or verif(char,0,3,6) or verif(char,1,4,7) or verif(char,2,5,8) or verif(char, 0,4, 8) or verif(char,2,4,6) :
                return True

c = 0
i = 0
while i<9:

        while True:
                pozitie_aleasa1 = input("Pozitia pe care jucatorul 1 vrea sa introduca x: ")
                pozitie_aleasa1 = int(pozitie_aleasa1)
                if pozitii[pozitie_aleasa1] != 'x' and pozitii[pozitie_aleasa1] !='0' :
                        pozitii[pozitie_aleasa1] ='x'
                        break
                else:
                        print("Pozitia e luata! ")
                        pass
        char = 'x'
        if verificare(char) == True:
                print("Jucatorul 1 a castigat! ")
                c = 1
                break
        tabela()
        i = i+1

        while True :
                pozitie_aleasa2 = input("Pozitia pe care jucatorul 2 vrea sa introduca 0: ")
                pozitie_aleasa2 = int(pozitie_aleasa2)
                if pozitii[pozitie_aleasa2] !='0' and pozitii[pozitie_aleasa2] !='x':
                        pozitii[pozitie_aleasa2] ='0'
                        break
                else:
                        print("pozitia e luata! ")
                        pass
        char = '0'
        if verificare(char) == True:
                print("Jucatorul 2 a castigat! ")
                c = 1
                break
        tabela()
        i = i+1

if c == 0:
        print("Jocul este remiza!")

